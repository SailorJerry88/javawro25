package pl.sda.rafal.zientara.game.lesson4.paint;

public enum Tool {
    RECTANGLE,
    OVAL,
    LINE,
    TRIANGLE
}
